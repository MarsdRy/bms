package pers.yaoliguo.bms.control;

import java.util.ArrayList;
import java.util.List;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import pers.yaoliguo.bms.control.view.Message;
import pers.yaoliguo.bms.entity.SysMenu;
import pers.yaoliguo.bms.entity.SysRoleMenuKey;
import pers.yaoliguo.bms.entity.SysUser;
import pers.yaoliguo.bms.service.ISysMenuRoleService;
import pers.yaoliguo.bms.service.ISysUserService;
import pers.yaoliguo.bms.service.impl.SysMenuService;
import pers.yaoliguo.bms.uitl.EhcacheUtil;
import pers.yaoliguo.bms.uitl.PermissionCodeUtil;
import pers.yaoliguo.bms.uitl.StringHelper;

@Controller
@RequestMapping("/SysMenuRole")
public class SysMenuRoleControl extends BaseControl {

	@Autowired
	ISysMenuRoleService sysMenuRoleService;

    @Autowired
	ISysUserService sysUserService;
	
	@Autowired
	SysMenuService sysMenuService;
	//@RequiresPermissions(value=PermissionCodeUtil.ROLE_MENU)
	@RequestMapping("/getMenusByRoleId")
	@ResponseBody
	public Object getMenusByRoleId(String roleId) {
		SysRoleMenuKey rolemenu = new SysRoleMenuKey();
		rolemenu.setRoleId(roleId);
		List<SysRoleMenuKey> menus = new ArrayList<SysRoleMenuKey>();

		Message<SysRoleMenuKey> msg = new Message<SysRoleMenuKey>();
		try {
			menus = sysMenuRoleService.getrolemenus(rolemenu);

			msg.setResult("200");

		} catch (Exception e) {
			msg.setResult("500");
			msg.setInfo(e.getMessage());
		}
		msg.setDataList(menus);
		return msg;

	}

	//@RequiresPermissions(value=PermissionCodeUtil.ROLE_MENU)
	@RequestMapping("/skipMenuRolePage")
	public String skipMenuRolePage() {

		return "redirect:/views/rolemenu/rolemenuList.html";
	}

	//@RequiresPermissions(value=PermissionCodeUtil.ROLE_MENU)
	@RequestMapping(value = "saveMenuRole", method = { RequestMethod.POST })
	@ResponseBody
	public Object saveMenuRole(@RequestBody List<SysRoleMenuKey> sys) {

		Message<SysRoleMenuKey> msg = new Message<SysRoleMenuKey>();
		try {
			sysMenuRoleService.deleteByRoleId(sys.get(0));
			if(!("-1".equals(sys.get(0).getMenuId())&&sys.size() == 1)){
				for (SysRoleMenuKey key : sys) {
					sysMenuRoleService.insert(key);
				}
			}
			//重新设置ehcache的菜单缓存
			Subject currentUser = SecurityUtils.getSubject();
			//获取到角色id
			String roleId = sys.get(0).getRoleId();
			SysRoleMenuKey roleMenu = new SysRoleMenuKey();
			roleMenu.setRoleId(roleId);
			//根据角色id去查该角色所有的菜单
			List<SysRoleMenuKey> roleMenuList= sysMenuRoleService.getrolemenus(roleMenu);
			//该用户的菜单
			List<SysMenu> menuList = sysMenuService.selectByIds(roleMenuList);
			//将菜单放入缓存中
			EhcacheUtil.getInstance().put(EhcacheUtil.MENU_CACHE, (String)currentUser.getPrincipal(), menuList);
			SimpleAuthorizationInfo simpleAuthorInfo = new SimpleAuthorizationInfo();
			//将菜单的url作为权限编码，用来搜权
			for (SysMenu sysMenu : menuList) {
				if(!StringHelper.isNullOrEmpty(sysMenu.getUrl()))
				{
					simpleAuthorInfo.addStringPermission(sysMenu.getUrl());
				}
			}
			msg.setResult("200");
		} catch (Exception e) {
			// TODO: handle exception
			msg.setResult("500");
			msg.setInfo(e.getMessage());
		}
		return msg;
	}
}
