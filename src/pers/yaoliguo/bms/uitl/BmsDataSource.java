package pers.yaoliguo.bms.uitl;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/** 
* @author 作者 wangyi: 
* @version 创建时间：2017年7月31日 下午3:18:41 
* 类说明 
*/
@Target({ ElementType.TYPE,ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME) 
public  @interface BmsDataSource {
	 String value();
}
