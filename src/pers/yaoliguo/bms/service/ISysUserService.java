package pers.yaoliguo.bms.service;

import java.util.List;
import java.util.Map;

import pers.yaoliguo.bms.common.PagerBean;
import pers.yaoliguo.bms.entity.SysUser;


/**
 * @ClassName:       ISysUserService
 * @Description:    TODO
 * @author:            yao
 * @date:            2017年6月21日        下午9:36:33
 */
public interface ISysUserService {

    int deleteByPrimaryKey(String id);

    int insert(SysUser record);

    int insertSelective(SysUser record);

    SysUser selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(SysUser record);

    int updateByPrimaryKey(SysUser record);
    
    List<SysUser> selectAllByPage(SysUser record,PagerBean page);
    
    List<SysUser> selectAll(SysUser record);
    
    List<SysUser> selectAll(Map map);
    
    int selectCount(SysUser record, PagerBean page);

}
