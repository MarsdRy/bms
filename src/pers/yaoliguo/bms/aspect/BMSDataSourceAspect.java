package pers.yaoliguo.bms.aspect;

import java.lang.reflect.Method;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import pers.yaoliguo.bms.uitl.BmsDataSource;

/** 
* @author 作者 wangyi: 
* @version 创建时间：2017年7月31日 下午3:32:45 
* 类说明 
*/
@Component
@Aspect
public class BMSDataSourceAspect {
	/**
	 * 拦截目标方法，获取BMSdatasource注解中的数据源。
	 * 
	 */
	@Pointcut("execution(* pers.yaoliguo.bms.service.activity.*.*(..)) and execution(* pers.yaoliguo.bms.service.impl.activity.*.*(..))")
	public void pointcut(){}
	
	@Before("pointcut()")
	public void intercept(JoinPoint point) throws Exception {
        Class<?> target = point.getTarget().getClass();
        MethodSignature signature = (MethodSignature) point.getSignature();
        // 默认使用目标类型的注解，如果没有则使用其实现接口的注解
        for (Class<?> clazz : target.getInterfaces()) {
            resolveDataSource(clazz, signature.getMethod());
        }
        resolveDataSource(target, signature.getMethod());
    }
     
	  /**
     * 提取目标对象方法注解和类型注解中的数据源标识
     * 
     * @param clazz
     * @param method
     */
   private void resolveDataSource(Class<?> clazz, Method method) {
       try {
           Class<?>[] types = method.getParameterTypes();
           // 默认使用类型注解
           if (clazz.isAnnotationPresent(BmsDataSource.class)) {
        	   BmsDataSource source = clazz.getAnnotation(BmsDataSource.class);
               DynamicDataSourceHolder.setDataSource(source.value());
           }
           // 方法注解可以覆盖类型注解
           Method m = clazz.getMethod(method.getName(), types);
           if (m != null && m.isAnnotationPresent(BmsDataSource.class)) {
        	   BmsDataSource source = m.getAnnotation(BmsDataSource.class);
               DynamicDataSourceHolder.setDataSource(source.value());
           }
       } catch (Exception e) {
           System.out.println(clazz + ":" + e.getMessage());
       }
   }
}
